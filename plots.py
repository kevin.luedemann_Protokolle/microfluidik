#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib as mt
mt.use("TKagg")
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
import matplotlib.pyplot as plt


def main():
    x = np.array([0.0,0.25,0.5,0.75,1.0])*0.1
    xerr = np.array([0.25,0.5,0.5,0.5,0.25])*0.025
    versch = np.array([1.0,0.0,0.0,0.0,-1.0])
    cylinder502020 = np.array([[[4.2,7.2,0.0,6.6,4.0],
                            [3.1,6.0,3.1,6.0,3.0],
                            [2.1,5.0,5.6,5.0,2.4]],
                            [[0.7,0.2,0.0,0.3,1.0],
                            [0.9,2.0,0.8,1.0,2.0],
                            [0.4,0.6,0.7,0.5,0.8]]])

    cylinder100520 = np.array([[[6.0,15.0,0.0,14.0,6.0],
                            [5.0,10.0,9.0,11.0,6.0],
                            [4.0,9.0,11.0,8.0,5.0]],
                            [[4.0,1.0,0.0,2.0,4.0],
                            [1.0,2.0,2.0,2.0,2.0],
                            [1.0,2.0,2.0,1.0,4.0]]])

    cylinder400220 = np.array([[[0.0,50.0,0.0,50.0,20.0],
                            [17.0,43.0,30.0,40.0,15.0],
                            [16.0,40.0,40.0,40.0,20.0]],
                            [[0.0,10.0,0.0,20.0,10.0],
                            [3.0,4.0,10.0,20.0,3.0],
                            [3.0,10.0,10.0,10.0,3.0]]])
    #50
    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x+versch*xerr,cylinder502020[0,0,:],yerr=cylinder502020[1,0,:],xerr=xerr,fmt='r+--',label='cylinder')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('A',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder502020[0,0,:])-np.max(cylinder502020[1,0,:])-0.01,np.max(cylinder502020[0,0,:])+np.max(cylinder502020[1,0,:])+0.01])
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    plt.tight_layout()
    #plt.legend(loc='best')
    plt.savefig('cylinder502020_cylinder.png',dpi=300)

    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x+versch*xerr,cylinder502020[0,1,:],yerr=cylinder502020[1,1,:],xerr=xerr,fmt='b+--',label='before')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('B',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder502020[0,0,:])-np.max(cylinder502020[1,0,:])-0.01,np.max(cylinder502020[0,0,:])+np.max(cylinder502020[1,0,:])+0.01])
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    plt.tight_layout()
    #plt.legend(loc='best')
    plt.savefig('cylinder502020_before.png',dpi=300)

    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x+versch*xerr,cylinder502020[0,2,:],yerr=cylinder502020[1,2,:],xerr=xerr,fmt='k+--',label='free')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('C',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder502020[0,0,:])-np.max(cylinder502020[1,0,:])-0.01,np.max(cylinder502020[0,0,:])+np.max(cylinder502020[1,0,:])+0.01])
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    plt.tight_layout()
    #plt.legend(loc='best')
    plt.savefig('cylinder502020_free.png',dpi=300)

    #100
    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x+versch*xerr,cylinder100520[0,0,:],yerr=cylinder100520[1,0,:],xerr=xerr,fmt='r+--',label='cylinder')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('A',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder100520[0,0,:])-np.max(cylinder100520[1,0,:])-0.01,np.max(cylinder100520[0,0,:])+np.max(cylinder100520[1,0,:])+0.01])
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    plt.tight_layout()
    #plt.legend(loc='best')
    plt.savefig('cylinder100520_cylinder.png',dpi=300)

    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x+versch*xerr,cylinder100520[0,1,:],yerr=cylinder100520[1,1,:],xerr=xerr,fmt='b+--',label='before')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('B',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder100520[0,0,:])-np.max(cylinder100520[1,0,:])-0.01,np.max(cylinder100520[0,0,:])+np.max(cylinder100520[1,0,:])+0.01])
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    plt.tight_layout()
    #plt.legend(loc='best')
    plt.savefig('cylinder100520_before.png',dpi=300)

    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x+versch*xerr,cylinder100520[0,2,:],yerr=cylinder100520[1,2,:],xerr=xerr,fmt='k+--',label='free')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('C',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder100520[0,0,:])-np.max(cylinder100520[1,0,:])-0.01,np.max(cylinder100520[0,0,:])+np.max(cylinder100520[1,0,:])+0.01])
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    plt.grid('on')
    plt.tight_layout()
    #plt.legend(loc='best')
    plt.savefig('cylinder100520_free.png',dpi=300)

    #400
    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x[1:]+versch[1:]*xerr[1:],cylinder400220[0,0,1:],yerr=cylinder400220[1,0,1:],xerr=xerr[1:],fmt='r+--',label='cylinder')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('A',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder400220[0,0,:])-np.max(cylinder400220[1,0,:])-0.01,np.max(cylinder400220[0,0,:])+np.max(cylinder400220[1,0,:])+0.01])
    plt.tight_layout()
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    #plt.legend(loc='best')
    plt.savefig('cylinder400220_cylinder.png',dpi=300)

    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x+versch*xerr,cylinder400220[0,1,:],yerr=cylinder400220[1,1,:],xerr=xerr,fmt='b+--',label='before')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('B',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder400220[0,0,:])-np.max(cylinder400220[1,0,:])-0.01,np.max(cylinder400220[0,0,:])+np.max(cylinder400220[1,0,:])+0.01])
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    plt.tight_layout()
    #plt.legend(loc='best')
    plt.savefig('cylinder400220_before.png',dpi=300)

    f = plt.figure()
    ax = f.gca()
    ax.errorbar(x+versch*xerr,cylinder400220[0,2,:],yerr=cylinder400220[1,2,:],xerr=xerr,fmt='k+--',label='free')
    plt.xlabel('position in channel [mm]',fontsize=28)
    plt.ylabel('velocity [mm/sec]',fontsize=28)
    plt.title('C',fontsize=28)
    plt.grid('on')
    plt.tick_params(axis='both', which='minor', labelsize=28)
    plt.tick_params(axis='both', which='major', labelsize=28)
    ax.set_ylim([np.min(cylinder400220[0,0,:])-np.max(cylinder400220[1,0,:])-0.01,np.max(cylinder400220[0,0,:])+np.max(cylinder400220[1,0,:])+0.01])
    ax.set_xlim([np.min(x)-np.max(xerr),np.max(x)+np.max(xerr)])
    plt.tight_layout()
    #plt.legend(loc='best')
    plt.savefig('cylinder400220_free.png',dpi=300)

    #plt.show()


if __name__ == "__main__":
    main()
