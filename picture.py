#!/usr/bin/python
import numpy as np
import scipy.misc as sm
import matplotlib.pyplot as plt
from glob import glob
from matplotlib_scalebar.scalebar import ScaleBar

def main():
    path = "/home/luedemann/_Puffer/Annika_Kevin161116/cylinder/"
    #imgs = ["cylinder_50l_20ms_20x_0065.jpg","cylinder_50l_20ms_20x_0078.jpg"]
    #imgs = glob('Auto_*')

    #pics = [path+img for img in imgs]
    #fram = [sm.imread(img) for img in pics]
    #fram = [sm.imread(img) for img in imgs]
    #preprocessing
    #filtered = np.copy(fram)
    #print filtered.shape

    #for i in range(len(filtered)):
    #    filtered[i,filtered[i]<(0.995*np.max(filtered[i]))] = 0.0

    #saving the sum
    #result = filtered[0]
    #for i in range(1,len(filtered)):
    #    result += filtered[i]
    #result = np.invert(result)
    #result[result<(0.75*np.max(filtered[i]))] = 0.0  

    #sm.imsave('result.png',result)
    #print np.max(result),np.min(result)
    """
    f = plt.figure()
    plt.imshow(filtered[0],interpolation='nearest')
    plt.colorbar()
    f = plt.figure()
    plt.imshow(fram[0],interpolation='nearest')
    plt.colorbar()
    f = plt.figure()
    plt.imshow(filtered[1],interpolation='nearest')
    plt.colorbar()
    f = plt.figure()
    plt.imshow(fram[1],interpolation='nearest')
    plt.colorbar()
    """
    f = plt.figure()
    plt.imshow(sm.imread('konti_A4_50l_40ms_10x_kontur_0124.jpg'),interpolation='nearest')
    scalebar = ScaleBar(3.657e-4*2.0, 'mm') # 1 pixel = 0.2 feet
    plt.gca().add_artist(scalebar)
    plt.axis('off')
    #plt.colorbar()
    plt.savefig('konti_kontur.png',dpi=300, bbox_inches='tight')
    #plt.show()


if __name__ == "__main__":
    main()

